var startDate = drupalSettings.meet_on_time.startdate;
var endDate = drupalSettings.meet_on_time.enddate;

var startDateObject = new Date(startDate);
var endDateObject = new Date(endDate);

console.log('Start date:', startDateObject);
console.log('End date:', endDateObject);  

const daysTag = document.querySelector(".days");

const currentDate = document.querySelector(".current-date");

const prevNextIcon = document.querySelectorAll(".icons span");

const successMessage = document.getElementById("success-message");

const selectedDateElement = document.getElementById("selected-date");

const selectedclickeventElement = document.getElementById("click_event");

let date = new Date();
let currYear = date.getFullYear();
let currMonth = date.getMonth();

const months = ["January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"];

const renderCalendar = () => {
    console.log('xjabxbxxbxbx');
    let firstDayofMonth = new Date(currYear, currMonth, 1).getDay();
    let lastDateofMonth = new Date(currYear, currMonth + 1, 0).getDate();
    let lastDayofMonth = new Date(currYear, currMonth, lastDateofMonth).getDay();
    let lastDateofLastMonth = new Date(currYear, currMonth, 0).getDate();
    let liTag = "";

    for (let i = firstDayofMonth; i > 0; i--) {
        liTag += `<li class="inactive">${lastDateofLastMonth - i + 1}</li>`;
    }

    // for (let i = 1; i <= lastDateofMonth; i++) {
    //     let isToday = i === date.getDate() && currMonth === date.getMonth() 
    //                  && currYear === date.getFullYear() ? "active" : "";
    //     liTag += `<li class="${isToday}" onclick="openEventPopup(${i})">${i}</li>`;
    // }
    for (let i = 1; i <= lastDateofMonth; i++) {
        let isToday = i === date.getDate() && currMonth === date.getMonth() 
                     && currYear === date.getFullYear() ? "active" : "";
        // liTag += `<li class="${isToday}" onclick="openEventPopup(${i})">${i}</li>`;
        let currentDate = new Date(currYear, currMonth, i);

        if (currentDate >= new Date(startDateObject.getFullYear(), startDateObject.getMonth(), startDateObject.getDate()) &&
        currentDate <= new Date(endDateObject.getFullYear(), endDateObject.getMonth(), endDateObject.getDate())) {
            liTag += `<li class="highlighted"  data-highlighted="true" onclick="openEventPopup(${i})">${i}</li>`;
        } 
        else {
            liTag += `<li class="${isToday}" onclick="openEventPopup(${i})">${i}</li>`;
        }
    }
    
    for (let i = lastDayofMonth; i < 6; i++) {
        liTag += `<li class="inactive">${i - lastDayofMonth + 1}</li>`
    }
    currentDate.innerText = `${months[currMonth]} ${currYear}`;
    daysTag.innerHTML = liTag;
};

renderCalendar();
console.log('vvzvc');
prevNextIcon.forEach(icon => {
    icon.addEventListener("click", () => {
        currMonth = icon.id === "prev" ? currMonth - 1 : currMonth + 1;

        if (currMonth < 0 || currMonth > 11) {
            date = new Date(currYear, currMonth, date.getDate());
            currYear = date.getFullYear();
            currMonth = date.getMonth();
        } else {
            date = new Date();
        }
        renderCalendar();
    });
});

function openEventPopup(day) {
    console.log('bdhs');
    const eventPopup = document.getElementById("event-popup");
    
    if (
      day >= startDateObject.getDate() &&
      currYear === startDateObject.getFullYear() &&
      day <= endDateObject.getDate() &&
      currYear === endDateObject.getFullYear() &&
      currMonth === startDateObject.getMonth() && 
      currMonth === endDateObject.getMonth() 
    ) {

      eventPopup.style.display = "block";
      selectedDateElement.textContent = `Book a slot on ${months[currMonth]} ${day}`;

      var date = `${months[currMonth]} ${day} ${currYear}`;

      jQuery('input[name="hidden_date_field"]').val(date);
}
    else {
      eventPopup.style.display = "none"; 
    }
}

function saveEvent() {
    console.log('bnbbn');
    const eventName = document.getElementById("event-name").value;
    const eventEmail = document.getElementById("event-email").value;
    const eventContact = document.getElementById("event-contact").value;
    const eventStartTime = document.getElementById("event-start-time").value;
    const eventEndTime = document.getElementById("event-end-time").value;

    successMessage.innerText = "Slot booked successfully!";

    setTimeout(() => {
        closeEventPopup();
        successMessage.innerText = ""; 
    }, 2000);
}

function closeEventPopup() {
    console.log('vvnnbvbv');
    const eventPopup = document.getElementById("event-popup");
    eventPopup.style.display = "none";
}


