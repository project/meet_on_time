<?php
namespace Drupal\meet_on_time\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database; 
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserTimeSlotBooking extends FormBase {
  /**
   * {@inheritdoc}
   */

   protected $configFactory;

  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  public function getFormId() {
    return 'user_tslot_book';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
 
  $start_time = \Drupal::state()->get("start_time");
  $end_time = \Drupal::state()->get("end_time");
  $start_date = \Drupal::state()->get("start_date");
  $end_date = \Drupal::state()->get("end_date");
  $time_duration = \Drupal::state()->get("time_duration");

  $start_time = date('h:i A',strtotime($start_time));
  $end_time = date('h:i A',strtotime($end_time));

   $time_slot_options = [];

     $start_time = strtotime($start_time);
     $end_time = strtotime($end_time);
     $duration = $time_duration ;
  
     $start_date = strtotime($start_date);
     $end_date = strtotime($end_date);
    
     $time_slots = array();
   
     while ($start_date <= $end_date) {
      
       $current_time = $start_time;
   
       $date_time_slots = array();
   
       while ($current_time < $end_time) {
         $slot_start_time = date('h:i A', $current_time); 

         $slot_end_time = date('h:i A', min($current_time + $duration * 60, $end_time));
         $date_time_slots[] = $slot_start_time . ' - ' . $slot_end_time;
        
         $current_time += $duration * 60;
   
         if ($current_time > $end_time) {
           break;
         }
       }
    $time_slots[date('Y-m-d', $start_date)] = $date_time_slots;
       
    $start_date += 86400;
    }
     foreach ($time_slots as $date => $slots) {
       foreach ($slots as $key => $slot) {
        $time_slot_options[$slot] = $slot;
       }
     }
     $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name:'),
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('label'),
      ),
    );
    $form['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email:'),
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('label'),
      ),
    );
    $form['phone_no'] = array (
      '#type' => 'tel',
      '#title' => $this->t('Phone Number:'),
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('label'),
      ),
    );
    $form['time_slot'] = [
      '#type' => 'select',
      '#title' => 'Select Time Slot',
      '#options' => $time_slot_options,
      '#required' => TRUE,
      '#attributes' => array(    
        'class' => array('label'),
      ),
    ];
    $form['hidden_date_field'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'selected-date'],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
      '#button_type' => 'primary',
      '#attributes' => array(
        'class' => array('button--primary'),
      ),
    );
    return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
    $name = $form_state->getValue('name');
    if (!preg_match('/^[A-Za-z\s]+$/', $name)) {
      $form_state->setErrorByName('name', $this->t('Please enter a valid name containing only letters and spaces.'));
    }

    $phone_number = $form_state->getValue('phone_no');
    $phone_number = preg_replace('/[^0-9]/', '', $phone_number);
    if (strlen($phone_number) !== 10 || !is_numeric($phone_number)) {
      $form_state->setErrorByName('phone_no', $this->t('Please enter a valid 10-digit phone number.'));
    }
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {
      $conn = Database::getConnection();

      $formfield = $form_state->getValues();

      $formData['name'] = $formfield['name'];
      $formData['email'] = $formfield['email'];
      $formData['phone_no'] = $formfield['phone_no'];
      $formData['time_slot'] = $formfield['time_slot'];
      $dateString = $form_state->getValue('hidden_date_field'); 
      $timestamp = strtotime($dateString);
      $formData['hidden_date_field'] = date('Y-m-d', $timestamp);
        $conn->insert('user_timeslot_booking')
        ->fields($formData)
        ->execute(); 
  
      \Drupal::messenger()->addMessage($this->t("You Have Booked Slot Successfully"));
       
      $form_state->setRedirect('meet_on_time.gethostdata');
  }
}
