<?php

namespace Drupal\meet_on_time\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

class HostForm extends ConfigFormBase {

  /**
   * The form settings.
   *
   * @var \Drupal\Core\Form\ConfigFormBase
   */
     protected $settings;
    
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->settings,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'host_form';
  }

    /**
   * {@inheritdoc}
   */
    
   public function buildForm(array $form, FormStateInterface $form_state) {

    $default_name = \Drupal::state()->get("name");
    $default_meeting_title = \Drupal::state()->get("meeting_title");
    $default_email = \Drupal::state()->get("email");
    $default_phone_no = \Drupal::state()->get("phone_no");
    $default_start_time = \Drupal::state()->get("start_time");
    $default_end_time = \Drupal::state()->get("end_time");
    $default_start_date = \Drupal::state()->get("start_date");
    $default_end_date = \Drupal::state()->get("end_date");
    $default_time_duration = \Drupal::state()->get("time_duration");

    $form['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name:'),
        '#required' => TRUE,
        '#default_value' => $default_name,
    ];
    
    $form['meeting_title'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Meeting Title:'),
        '#required' => TRUE,
        '#default_value' => $default_meeting_title,
    );

    $form['email'] = array(
        '#type' => 'email',
        '#title' => $this->t('Email:'),
        '#required' => TRUE,
        '#default_value' => $default_email,
    );

    $form['phone_no'] = array (
        '#type' => 'tel',
        '#title' => $this->t('Phone Number:'),
        '#required' => TRUE,
        '#default_value' => $default_phone_no,
    );

    $form['start_time'] = array(
        '#type' => 'datetime',
        '#title' => $this->t('Start time of the meeting:'),
        '#date_time_format' => 'H:i:s',
        '#date_date_element' => 'none', 
        '#date_time_element' => 'time',
        '#required' => TRUE,
        '#default_value' => $default_start_time,
    );
    
    $form['end_time'] = array(
        '#type' => 'datetime',
        '#title' => $this->t('End time of the meeting:'),
        '#date_time_format' => 'H:i:s',
        '#date_date_element' => 'none', 
        '#date_time_element' => 'time', 
        '#required' => TRUE,
        '#default_value' => $default_end_time,
    );
    $form['start_date'] = array (
        '#type' => 'date',
        '#title' => $this->t('Start date of the meeting:'),
        '#date_date_format' => 'Y-m-d',
        '#required' => TRUE,
        '#default_value' => $default_start_date,
        '#element_validate' => [[$this, 'validateStartDate']],
    );
    $form['end_date'] = array (
        '#type' => 'date',
        '#title' => $this->t('End date of the meeting:'),
        '#date_date_format' => 'Y-m-d',
        '#required' => TRUE,
        '#default_value' => $default_end_date,
        '#element_validate' => [[$this, 'validateEndDate']],
    );
    $form['time_duration'] = array (
        '#type' => 'select',
        '#title' => ('Time Duration:'),
        '#options' => array(
          'selectitems' => $this->t('select----'),
          '60min' => $this->t('60 min'),
          '120min' => $this->t('120 min'),
          '180min' => $this->t('180 min'),
          '45min' => $this->t('45 min'),
          '30min' => $this->t('30 min'),
          '15min' => $this->t('15 min'),
        ),
        '#default_value' => $default_time_duration,
    );
    $form['generate_slots'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Generate Time Slots'),
        '#submit' => ['::generateTimeSlots'],
        '#ajax' => [
          'callback' => '::generateTimeSlotsAjaxCallback',
          'wrapper' => 'time-slots-wrapper',
        ],
    );
    $form['time_slots_wrapper'] = array(
        '#type' => 'container',
        '#attributes' => array('id' => 'time-slots-wrapper'),
    );
    $form['time_slots_wrapper']['time_slots'] = array(
        '#markup' => '',
    );
    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);  }
  
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
   
    $name = $form_state->getValue('name');
    if (!preg_match('/^[A-Za-z\s]+$/', $name)) {
      $form_state->setErrorByName('name', $this->t('Please enter a valid name containing only letters and spaces.'));
    }
  
    $phone_number = $form_state->getValue('phone_no');
    $phone_number = preg_replace('/[^0-9]/', '', $phone_number);
    if (strlen($phone_number) !== 10 || !is_numeric($phone_number)) {
      $form_state->setErrorByName('phone_no', $this->t('Please enter a valid 10-digit phone number.'));
    }
    
  }
  public function validateStartDate($element, FormStateInterface $form_state) {
    $selectedStartDate = $form_state->getValue('start_date');
    $currentDate = date('Y-m-d');
    if ($selectedStartDate < $currentDate) {
      $form_state->setError($element, $this->t('Meeting cannot be scheduled for past dates...!'));
    }
  }
  public function validateEndDate($element, FormStateInterface $form_state) {
    $selectedStartDate = $form_state->getValue('end_date');
    $currentDate = date('Y-m-d');
    if ($selectedStartDate < $currentDate) {
      $form_state->setError($element, $this->t('Meeting cannot be scheduled for past dates...!'));
    }
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();
      \Drupal::state()->set("name", $form_values['name']);
      \Drupal::state()->set("meeting_title", $form_values['meeting_title']);
      \Drupal::state()->set("email", $form_values['email']);
      \Drupal::state()->set("phone_no", $form_values['phone_no']);
      \Drupal::state()->set("start_time", $form_values['start_time']);
      \Drupal::state()->set("end_time", $form_values['end_time']);
      \Drupal::state()->set("start_date", $form_values['start_date']);
      \Drupal::state()->set("end_date", $form_values['end_date']);
      \Drupal::state()->set("time_duration", $form_values['time_duration']);
      \Drupal::messenger()->addMessage($this->t('Configuration settings have been saved successfully'));
  }
  public function generateTimeSlots(array &$form, FormStateInterface $form_state) {
    $start_time = strtotime($form_state->getValue('start_time'));
    $end_time = strtotime($form_state->getValue('end_time'));
    $duration = $form_state->getValue('time_duration');

    $start_date = strtotime($form_state->getValue('start_date'));
    $end_date = strtotime($form_state->getValue('end_date'));

    $time_slots = array();

    while ($start_date <= $end_date) {
      $current_time = $start_time;
      $date_time_slots = array();

      while ($current_time < $end_time) {
        $slot_start_time = date('h:i A', $current_time); 
        $slot_end_time = date('h:i A', min($current_time + $duration * 60, $end_time));
        $date_time_slots[] = $slot_start_time . ' - ' . $slot_end_time;
        
        $current_time += $duration * 60;
        if ($current_time > $end_time) {
          break;
        }
      }
      
      $time_slots[date('Y-m-d', $start_date)] = $date_time_slots;
     
      $start_date += 86400; 
    }
    $table_markup = '';
    foreach ($time_slots as $date => $slots) {
      $table_markup .= '<h2>' . $date . '</h2>';
      $table_markup .= '<table><tr><th>Time Slots</th></tr>';
      $arr[] = $slots;
      foreach ($slots as $slot) {
        $table_markup .= '<tr><td>' . $slot . '</td></tr>';
      }
      $table_markup .= '</table>';
    }
    $form['time_slots_wrapper']['time_slots']['#markup'] = '<div class="time-slots">' . $table_markup . '</div>';
    \Drupal::messenger()->addMessage($this->t("Time slots have been generated successfully and are displayed below."));
  }
  public function generateTimeSlotsAjaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['time_slots_wrapper'];
  }
}

