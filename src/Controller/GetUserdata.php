<?php

namespace Drupal\meet_on_time\Controller;

use Drupal\Core\Controller\ControllerBase;

class GetUserData extends ControllerBase {

  public function userData() {

    $query = \Drupal::database()->select('user_timeslot_booking', 'uid')
      ->fields('uid')
      ->execute()
      ->fetchAll();

    $details = [];
    foreach ($query as $row) {
      $details[] = [
        'name' => $row->name,
        'email' => $row->email,
        'phone_no' => $row->phone_no,
        'time_slot' => $row->time_slot,
        'hidden_date_field' => $row->hidden_date_field,
      ];
    }
    $output = [
      '#theme' => 'get__user_data', 
      '#details' => $details ,
    ];
    return $output;
  }
}

