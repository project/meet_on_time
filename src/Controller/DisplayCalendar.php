<?php

namespace Drupal\meet_on_time\Controller;

use Drupal\Core\Controller\ControllerBase;

class DisplayCalendar extends ControllerBase {

  public function hostData() {

    $data = [];

    $name = \Drupal::state()->get("name");
    $meeting_title = \Drupal::state()->get("meeting_title");
    $email = \Drupal::state()->get("email");
    $phone_no = \Drupal::state()->get("phone_no");
    $start_time = \Drupal::state()->get("start_time");
    $end_time = \Drupal::state()->get("end_time");
    $start_date = \Drupal::state()->get("start_date");
    $end_date = \Drupal::state()->get("end_date");
    $time_duration = \Drupal::state()->get("time_duration");

    $starttime = date('h:i A',strtotime($start_time));
    $endtime = date('h:i A',strtotime($end_time));

    $data['name'] = isset($name) ? $name : '';
    $data['meeting_title'] = isset($meeting_title) ? $meeting_title : '';
    $data['email'] = isset($email) ? $email : '';
    $data['phone_no'] = isset($phone_no) ? $phone_no : '';
    $data['start_time'] = isset($starttime) ? $starttime : '';
    $data['end_time'] = isset($endtime) ? $endtime : '';
    $data['start_date'] = isset($start_date) ? $start_date : '';
    $data['end_date'] = isset($end_date) ? $end_date : '';
    $data['time_duration'] = isset($time_duration) ? $time_duration : '';

    return [
      '#theme' => 'display__formin_calendar',
      '#data' =>   $data,
      '#form' => \Drupal::formBuilder()->getForm('Drupal\meet_on_time\Form\UserTimeSlotBooking'),
      '#attached' => [
        'library' => [
          'meet_on_time/meet_on_time_display_calendar',
        ],
        'drupalSettings' => [
          'meet_on_time' => [
            'startdate' => $data['start_date'],
            'enddate' => $data['end_date'],
          ],
        ],
      ],
    ];
  }
}