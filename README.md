# Meet On Time Module

- A meet-on-time module provides to schedule meeting on desired date & time and user can book desire slot.

## Table of contents

- Introduction
- Description
- Requirements
- Installation
- Configuration
- Maintainers

# Introduction #

The Meet On Time module is designed to facilitate the scheduling of meetings 
by hosts on a Drupal website. It allows hosts to set up meeting slots at specific 
times, and users can then choose from these slots to attend the meeting.

# Description # 

Once you install this module than folow sthese steps:
  1. Meeting Scheduling: Hosts can schedule meetings for desired date and time. (/host-form).
  2. User Selection: Users can choose  schedule and suitable time slot to attend the meeting(/book-meeting-slot).
  4. Customization: Hosts have the flexibility to customize meeting details and user can book slot for multiple times.

# Requirements #

Drupal 8, 9, 10.
This module requires no modules outside of Drupal core.

# Installation #

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

# Configuration #

- Once the module has been installed, navigate to
  (/host-form)for schedule meeting.
- To book slot navigate to (/book-meeting-slot) and click on highlighted date scheduled by host.
- To see User details navigate to (/user-info-table).

# Maintainers #

- Krati Arya - [krati-arya](https://www.drupal.org/u/krati-arya)
- Chanchal Tiwari - [chanchal-tiwari](https://www.drupal.org/u/chanchal-tiwari)
- Vijay Sharma - [vijaysharma_89](https://www.drupal.org/u/vijaysharma_89)
- priyansh-chourasiya - [priyansh-chourasiya](https://www.drupal.org/u/priyansh-chourasiya)